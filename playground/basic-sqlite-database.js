var Sequelize = require('sequelize');
var sequelize = new Sequelize(undefined, undefined, undefined, {
    'dialect': 'sqlite',
    'storage': __dirname + '/basic-sqlite-database.sqlite'
});

var Availability = sequelize.define('availability', {
    day: {
        type: Sequelize.STRING
    },
    time: {
        type: Sequelize.STRING
    }
});

var Employee = sequelize.define('employee', {
    name: Sequelize.STRING
});

Availability.belongsTo(Employee);
Employee.hasMany(Availability);

sequelize.sync({
    force: true
}).then(function() {
    console.log('Everything is synced');

    // Employee.findById(1).then(function (employee) {
    //     employee.getAvailabilities({
    //         // where: {
    //         //     completed: false
    //         // }
    //     }).then(function (availabilities) {
    //         availabilities.forEach(function (availability) {
    //             console.log(availability.toJSON());
    //         });
    //     });
    // });

    Employee.findAll().then(function (employees) {
        if (employees) {
            console.log(employees);
        } else {
            console.log('Todo not found');
        }
    });

    // Employee.create({
    //     name: 'David'
    // }).then(function () {
    //     return Availability.create({
    //         day: 'Tuesday',
    //         time: '10.00-11.00'
    //     });
    // }).then(function (availability) {
    //     Employee.findById(1).then(function (employee) {
    //         employee.addAvailability(availability);
    //     });
    // });
});
