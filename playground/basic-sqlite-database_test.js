var Sequelize = require('sequelize');
var sequelize = new Sequelize(undefined, undefined, undefined, {
    'dialect': 'sqlite',
    'storage': __dirname + '/basic-sqlite-database.sqlite'
});

var Availability = sequelize.define('availability', {
    day: {
        type: Sequelize.STRING
    },
    time: {
        type: Sequelize.STRING
    }
});

var User = sequelize.define('user', {
    name: Sequelize.STRING
});

Availability.belongsTo(User);
User.hasMany(Availability);

sequelize.sync({
    force: true
}).then(function() {
    console.log('Everything is synced');

    // User.findById(1).then(function (user) {
    //     user.getTodos({
    //         where: {
    //             completed: false
    //         }
    //     }).then(function (todos) {
    //         todos.forEach(function (todo) {
    //             console.log(todo.toJSON());
    //         });
    //     });
    // });

    User.create({
        name: 'andrew@example.com'
    }).then(function () {
        return Availability.create({
            day: 'Clean yard'
        });
    }).then(function (availability) {
        User.findById(1).then(function (user) {
            user.addAvailability(availability);
        });
    });
});
