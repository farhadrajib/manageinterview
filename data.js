// --------------------------------------------------------------------
var managers_new = {
id: 1,
name: 'Angela Walker',
availability: [{
    day:'Monday',
    time: '10.00-12.00'
    },
    {
    day: 'Wednesday',
    time: '13.00-15.30'
    }]    
}

var managers = [{
    id: 1,
    name: 'Angela Walker',
    availability:{
        day: ['Monday', 'Wednesday'],
        time: ['10.00-12.00', '13.00-15.30']
    }
},
{
    id: 2,
    name: 'Lia Shelton',
    availability:{
        day: ['Tuesday', 'Wednesday', 'Friday'],
        time: ['10.00-12.00', '13.00-15.30', '14.00-16.00']
    }
}];
var candidates = [{
    id: 1,
    name: 'Anna Bass',
    availability:{
        day: ['Monday', 'Friday'],
        time: ['10.00-12.00', '11.00-12.00']
    }
},
{
    id: 2,
    name: 'Darrell Gill',
    availability:{
        day: ['Wednesday', 'Thursday'],
        time: ['13.30-14.30', '13.00-15.30']
    }
}];
// .......................................................................

var availability={
    "day" : "Wednesday",
    "time" : "12.00-13.00",
    "employeeId" : "1"
}