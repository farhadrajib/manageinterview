var express = require('express');
var bodyParser = require('body-parser');
var _ =  require('underscore');
var db = require('./db.js');

var app = express();
var PORT = process.env.PORT || 3000;
var managers = [];
var candidates = [];
var managerNextId = 1;
var candidateNextId = 1;

app.use(bodyParser.json());

//..................... GET requests .........................
app.get('/', function(req, res){
    res.send('Welcome to manage interview app');
});
//GET all managers
app.get('/managers', function(req, res){

    db.employee.findAll().then(function (employees) {
        res.json(employees);
    }, function (e) {
        res.status(500).send();
    });
});
//GET all candidates
app.get('/candidates', function(req, res){
    res.json(candidates);
});

//GET all availabilities
app.get('/availabilities', function(req, res){

    db.availability.findAll().then(function (availabilities) {
        res.json(availabilities);
    }, function (e) {
        res.status(500).send();
    });
});

//GET manager by id
app.get('/managers/:id', function(req, res){
    var managerId = parseInt(req.params.id, 10);
    // var matchedManager = _.findWhere(managers,{id: managerId});
    // if(matchedManager){
    //     res.json(matchedManager);
    // }else{
    //     res.status(404).send();
    // }

    db.employee.findById(managerId).then(function (employee) {
        res.json(employee.toJSON());
    }, function (e) {
        res.status(404).json(e);
    });

});

//GET employee by employee id
app.get('/managers/:id', function(req, res){
    var managerId = parseInt(req.params.id, 10);
    // var matchedManager = _.findWhere(managers,{id: managerId});
    // if(matchedManager){
    //     res.json(matchedManager);
    // }else{
    //     res.status(404).send();
    // }

    db.employee.findById(managerId).then(function (employee) {
        res.json(employee.toJSON());
    }, function (e) {
        res.status(404).json(e);
    });

});

//GET availability by employee id
app.get('/availability/:id', function(req,res){
    var employeeId = parseInt(req.params.id);

    db.employee.findById(employeeId).then(function (employee) {
        employee.getAvailabilities({
            // where: {
            //     completed: false
            // }
        }).then(function (availabilities) {
            res.json(availabilities);
        });
    });



});
//..................... end of GET requests .........................

//..................... POST requests .........................

// POST new managers
app.post('/managers', function(req,res){

    var body = _.pick(req.body, 'name');


    db.employee.create(body).then(function (employee) {
        res.json(employee.toJSON());
    }, function (e) {
        res.status(400).json(e);
    });

    // trim name, check for string, non-zero length,
    // check availability.day for array, empty
    // check availability.time for array, empty

    // if(_.isString(body.name) || _.isArray(body.availability.day) || _.isArray(body.availability.time ||
    // body.availability.day.length == 0 || body.availability.time.length == 0)){
    // if(!_.isString(body.name)){
    //     return res.status(400).send();
    // }
    // // add id field
    // body.id = managerNextId++;
    // // push body into array
    // managers.push(body);
    // res.json(body);
});

// POST new availability
app.post('/availability', function(req,res){

    var body = _.pick(req.body, 'day', 'time');
    var employee = _.pick(req.body, 'employeeId');;
    var employeeId = parseInt(employee.employeeId);


    db.availability.create(body).then(function (availability) {
        db.employee.findById(employeeId).then(function (employee) {
            employee.addAvailability(availability);
            res.json(availability.toJSON());
        });
    }, function (e) {
        res.status(400).json(e);
    });

});


//POST new candidates
app.post('/candidates', function(req,res){
    var body = _.pick(req.body, 'name', 'availability');

    // trim name, check for string, non-zero length,
    // check availability.day for array, empty
    // check availability.time for array, empty

    // if(_.isString(body.name) || _.isArray(body.availability.day) || _.isArray(body.availability.time ||
    // body.availability.day.length == 0 || body.availability.time.length == 0)){

    if(!_.isString(body.name)){
        return res.status(400).send();
    }

    // add id field
    body.id = candidateNextId++;
    // push body into array
    candidates.push(body);
    res.json(body);

});
//..................... end of POST requests .........................

//..................... DELETE requests .........................

// DELETE request for managers by id
app.delete('/managers/:id', function(req, res){
    var managerId = parseInt(req.params.id, 10);
    var matchedManager = _.findWhere(managers, {id: managerId});

    if(!matchedManager){
        res.status(404).json({"error": "no manager found with that id"});
    }else{
        managers = _.without(managers, matchedManager);
        res.json(matchedManager);
    }


});

// DELETE request for candidates by id
app.delete('/candidates/:id', function(req,res){
    var candidateId = parseInt(req.params.id);
    var matchedCandidate = _.findWhere(candidates, {id: candidateId});

    if(!matchedCandidate){
        res.status(404).json({"error": "no candidate found with that id"});
    }else{
        candidates = _.without(candidates, matchedCandidate);
        res.json(matchedCandidate);
    }
});
//..................... end of DELETE requests .........................


//..................... PUT requests .........................
// PUT requests for managers
app.put('/managers/:id', function(req,res){
    var managerId = parseInt(req.params.id, 10);

    var body = _.pick(req.body, 'name');

    var attributes = {};

    if (body.hasOwnProperty('name')) {
        attributes.name = body.name;
    }

    db.employee.findOne({
        where: {
            id: managerId
        }
    }).then(function(employee) {
        if (employee) {
            employee.update(attributes).then(function(employee) {
                res.json(employee.toJSON());
            }, function(e) {
                res.status(400).json(e);
            });
        } else {
            res.status(404).send();
        }
    }, function() {
        res.status(500).send();
    });



    // if(!matchedManager){
    //     return res.status(404).send();
    // }
    // if(body.hasOwnProperty('name') && _.isString(body.name)){
    //     validAttributes.name = body.name;
    // }else if (body.hasOwnProperty('name')){
    //     return res.status(400).send();
    // }

    // // do validation for availability
    //
    // // update with valid attributes
    // _.extend(matchedManager, validAttributes);
    // res.json(matchedManager);
});

// PUT requests for availability by employee id
app.put('/availability/:id', function(req,res){
    var availabilityId = parseInt(req.params.id, 10);

    var body = _.pick(req.body, 'day', 'time');
    var employee = _.pick(req.body, 'employeeId');;
    var employeeId = parseInt(employee.employeeId);

    var attributes = {};

    if (body.hasOwnProperty('day')) {
        attributes.day = body.day;
    }

    if (body.hasOwnProperty('time')) {
        attributes.time = body.time;
    }

    db.availability.findOne({
        where: {
            id: availabilityId
        }
    }).then(function(availability) {
        if (availability) {
            availability.update(attributes).then(function(availability) {
                res.json(availability.toJSON());
            }, function(e) {
                res.status(400).json(e);
            });
        } else {
            res.status(404).send();
        }
    }, function() {
        res.status(500).send();
    });
});



// PUT requests for candidates
app.put('/candidates/:id', function(req,res){
    var candidateId = parseInt(req.params.id, 10);
    var matchedCandidate = _.findWhere(candidates, {id: candidateId});
    var body = _.pick(req.body, 'name', 'availability');
    var validAttributes = {};

    if(!matchedCandidate){
        return res.status(404).send();
    }
    if(body.hasOwnProperty('name') && _.isString(body.name)){
        validAttributes.name = body.name;
    }else if(body.hasOwnProperty('name'))
    {
        return res.status(400).send();
    }
    _.extend(matchedCandidate, validAttributes);
    res.json(matchedCandidate);

});


//..................... end of PUT requests .........................

// app.listen(PORT, function(){
//     console.log('Express server started');
// });

db.sequelize.sync().then(function() {
    app.listen(PORT, function() {
        console.log('Express listening on ' + PORT + '!');
    });
});