var Sequelize = require('sequelize');
var sequelize = new Sequelize(undefined, undefined, undefined, {
	'dialect': 'sqlite',
	'storage': __dirname + '/data/dev-manageinterview-api.sqlite'
});
var db = {};

db.employee = sequelize.import(__dirname + '/models/employees.js');
db.availability = sequelize.import(__dirname + '/models/availability.js');
db.sequelize = sequelize;
db.Sequelize = Sequelize;


db.availability.belongsTo(db.employee);
db.employee.hasMany(db.availability);

module.exports = db;