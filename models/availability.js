module.exports = function(sequelize, DataTypes) {
    return sequelize.define('availability', {
        day: {
            type: DataTypes.STRING
        },
        time: {
            type: DataTypes.STRING
        }
    });
};